﻿/*=============================================================================
Copyright (C) 2016 yumetodo <yume-wikijp@live.jp>
Distributed under the Boost Software License, Version 1.0.
(See http://www.boost.org/LICENSE_1_0.txt)
=============================================================================*/
#include "try_integer_parse.hpp"
template<typename T>
class integer_parse_iterator
#if __cplusplus < 201500 //C++17ではstd::iteratorは消える
	: public std::iterator<std::input_iterator_tag, T>
#endif
{
#if __cplusplus < 201500
private:
	using base_type = std::iterator<std::input_iterator_tag, T>;
public:
	using iterator_category = typename base_type::iterator_category;
	using value_type = typename base_type::value_type;
	using difference_type = typename base_type::difference_type;
	using pointer = typename base_type::pointer;
	using reference = typename base_type::reference;
#else
public:
	using iterator_category = typename std::iterator_traits<Iterator>::iterator_category;
	using value_type = T;
	using difference_type = std::ptrdiff_t;
	using pointer = T*;
	using reference = T&;
#endif
private:
	using lim = std::numeric_limits<value_type>;
	value_type re_;
	bool is_not_end_iterator_;
public:
	constexpr integer_parse_iterator() noexcept : re_(), is_not_end_iterator_(false) {}
	integer_parse_iterator(const integer_parse_iterator&) = default;
	integer_parse_iterator(integer_parse_iterator&&) = default;
	integer_parse_iterator& operator=(const integer_parse_iterator&) = default;
	integer_parse_iterator& operator=(integer_parse_iterator&&) = default;
	integer_parse_iterator(const std::string& s, value_type min = lim::min(), value_type max = lim::min()) noexcept
	{
		this->is_not_end_iterator_ = try_parse(s, this->re_, min, max);
	}
	integer_parse_iterator(const char* s, value_type min = lim::min(), value_type max = lim::min()) noexcept
	{
		this->is_not_end_iterator_ = try_parse(s, this->re_, min, max);
	}
	value_type operator*() const noexcept { return this->re_; }
	integer_parse_iterator& operator++() noexcept
	{
		this->re_ = 0;
		this->is_not_end_iterator_ = false;
		return *this;
	}
	integer_parse_iterator operator++(int) noexcept
	{
		const auto re = *this;
		this->re_ = 0;
		this->is_not_end_iterator_ = false;
		return re;
	}
	constexpr bool operator==(const integer_parse_iterator& r) const noexcept { return this->is_not_end_iterator_ == r.is_not_end_iterator_; }
	constexpr bool operator!=(const integer_parse_iterator& r) const noexcept { return !(*this == r); }
};
template<typename T>
class integer_parse_range {
public:
	using value_type = T;
	using iterator = integer_parse_iterator<value_type>;
private:
	using lim = std::numeric_limits<value_type>;
	iterator it_;
public:
	integer_parse_range() = delete;
	integer_parse_range(const integer_parse_range&) = delete;
	integer_parse_range(integer_parse_range&&) = default;
	integer_parse_range& operator=(const integer_parse_range&) = delete;
	integer_parse_range& operator=(integer_parse_range&&) = delete;
	integer_parse_range(const std::string& s, value_type min = lim::min(), value_type max = lim::min()) noexcept
		:it_(s, min, max)
	{}
	integer_parse_range(const char* s, value_type min = lim::min(), value_type max = lim::min()) noexcept
		:it_(s, min, max)
	{}
	iterator begin() noexcept { return it_; }
	constexpr iterator end() const noexcept { return{}; }
};
template<typename T>
inline integer_parse_range<T> integer_parse(const std::string& s, T min = std::numeric_limits<T>::min(), T max = std::numeric_limits<T>::max()) noexcept
{
	return{ s, min, max };
}
template<typename T>
inline integer_parse_range<T> integer_parse(const char* s, T min = std::numeric_limits<T>::min(), T max = std::numeric_limits<T>::max()) noexcept
{
	return{ s, min, max };
}
