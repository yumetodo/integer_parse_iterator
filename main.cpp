﻿/*=============================================================================
Copyright (C) 2016 yumetodo <yume-wikijp@live.jp>
Distributed under the Boost Software License, Version 1.0.
(See http://www.boost.org/LICENSE_1_0.txt)
=============================================================================*/
#include "integer_parse_iterator.hpp"
#include <cstdint>
#include <iostream>
int main()
{
	using std::string;
	std::cout << "try_parse" << std::endl;
	string s = "1123";
	{
		std::uint16_t re;
		if (try_parse(s, re)) {
			std::cout << re << std::endl;
		}
	}
	std::cout << "integer_parse_iterator(expect no output)" << std::endl;
	for (integer_parse_iterator<std::int8_t> it; it != integer_parse_iterator<std::int8_t>{}; ++it) {
		std::cout << *it << std::endl;
	}
	std::cout << "integer_parse" << std::endl;
	for (int n : integer_parse<int>(s)) {
		std::cout << n << std::endl;
	}
}
