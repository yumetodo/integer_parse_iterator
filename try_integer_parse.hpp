/*=============================================================================
Copyright (C) 2016 yumetodo <yume-wikijp@live.jp>
Distributed under the Boost Software License, Version 1.0.
(See http://www.boost.org/LICENSE_1_0.txt)
=============================================================================*/
#include <limits>
#include <cstdlib>
#include <climits>//in gcc
#include <cerrno>//in gcc
#include <cctype>
#include <type_traits>
#include <string>
template<bool cond> using concept_t = typename std::enable_if<cond, std::nullptr_t>::type;

template<typename T, concept_t<
	std::is_signed<T>::value && (sizeof(T) <= sizeof(long))
> = nullptr>
inline bool try_parse(const char* s, T& result, T min = std::numeric_limits<T>::min(), T max = std::numeric_limits<T>::max()) noexcept
{
	errno = 0;
	char* endptr;
	const long t = strtol(s, &endptr, 10);
	if (0 != errno || (0 == t && endptr == s) || t < min || max < t) {
		return false;
	}
	else {
		result = static_cast<T>(t);
		return true;
	}
}
inline bool try_parse(
	const char* s, long long& result,
	long long min = std::numeric_limits<long long>::min(),
	long long max = std::numeric_limits<long long>::max()
) noexcept
{
	errno = 0;
	char* endptr;
	const auto t = strtoll(s, &endptr, 10);
	if (0 != errno || (0 == t && endptr == s) || t < min || max < t) {
		return false;
	}
	else {
		result = t;
		return true;
	}
}
template<typename T, concept_t<
	std::is_unsigned<T>::value && (sizeof(T) <= sizeof(unsigned long))
> = nullptr>
inline bool try_parse(const char* s, T& result, T min = std::numeric_limits<T>::min(), T max = std::numeric_limits<T>::max()) noexcept
{
	errno = 0;
	char* endptr;
	const auto t = strtoul(s, &endptr, 10);
	if (0 != errno || (0 == t && endptr == s) || t < min || max < t) {
		return false;
	}
	else {
		result = static_cast<T>(t);
		return true;
	}
}
inline bool try_parse(
	const char* s, unsigned long long& result,
	unsigned long long min = std::numeric_limits<unsigned long long>::min(),
	unsigned long long max = std::numeric_limits<unsigned long long>::max()
) noexcept
{
	errno = 0;
	char* endptr;
	const auto t = strtoull(s, &endptr, 10);
	if (0 != errno || (0 == t && endptr == s) || t < min || max < t) {
		return false;
	}
	else {
		result = t;
		return true;
	}
}
template<typename T, concept_t<std::is_integral<T>::value> = nullptr>
inline bool try_parse(const std::string& s, T& result, T min = std::numeric_limits<T>::min(), T max = std::numeric_limits<T>::max()) noexcept
{
	return try_parse(s.c_str(), result, min, max);
}
